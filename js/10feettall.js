$(document).ready(function () {


  $(".read-more-button").on("click", function (e) {
    var closed = $(this).next(".closed");

    if ($(closed).is(":visible")) {
      $(closed).slideUp("slow", function () {});
      $(this).find(".read-more-icon").removeClass("rotated");
    } else {
      $(closed).slideDown("slow", function () {});
      $(this).find(".read-more-icon").addClass("rotated");
    }

    return false;
  });


  $(".exploding-link").click(function () {
    var href = $(this).attr("href");
    var width = $(this).find("img").width();
    var height = $(this).find("img").height();

    var position = $(this).offset();

    $(".masonry-images").css({ display: "hidden" });

    var src = $(this).find("img").attr("src");
    var img = $.parseHTML(
      '<div style="z-index:10000;background-repeat:no-repeat;background-size:cover;background-image:url(' +
        src +
        '); z-index: 100"></div>'
    );

    $(img).css({
      width: width,
      height: height,
      position: "absolute",
      left: position.left,
      top: position.top,
    });

    $(img).appendTo("body");

    $(img).animate(
      {
        width: "100vw",
        height: "100vh",
        "max-width": "100%",
        top: $(document).scrollTop(),
        left: 0,
      },
      2000,
      function () {
        window.location.href = href;
        //$(img).remove();
      }
    );

    return false;
  });


  $('#menu-icon').on('click touchstart', function () {

    if($(this).hasClass('open')) {
      MenuHide();
      $(this).removeClass('open');
    }
    else
    {
      MenuShow();
      $(this).addClass('open');
    }

  });




  MenuScroll();
  BounceArrow();

  BringHighResImageUp();

});


function BringHighResImageUp()
{
  var highRes = gsap.timeline();
  highRes.to($('.high-res'), 0, {opacity:1}, 2);
  highRes.play();
}


function BounceArrow() {
  $("#home-arrow").effect(
    "bounce",
    { times: 2, distance: 10 },
    1750,
    function () {
      setTimeout(BounceArrow, 0);
    }
  );
}


function MenuScroll() {
  window.onscroll = function (ev) {
    if (window.scrollY > window.innerHeight - 81)
    {
      this.document.getElementById("nav").classList.add("scrolled");

      if (window.innerHeight - window.scrollY > 0) {
        var opacity = 1 - (window.innerHeight - window.scrollY) / 81;
        document.getElementById("nav").style.opacity = opacity;
      }
      else
        document.getElementById("nav").style.opacity = 1;
    } 
    else if (window.scrollY > window.innerHeight) {
      document.getElementById("nav").style.opacity = 0;
    } 
    else {
      this.document.getElementById("nav").classList.remove("scrolled");
      document.getElementById("nav").style.opacity = 1;
    }

    //var marginTop = $(document).scrollTop() - $(window).height();
    //var newMarginTop = marginTop + $(window).height();

    //console.log('*window height is ' + $(window).height());
    //console.log('scroll is ' + $(document).scrollTop());
    //console.log('margin-top is ' + marginTop);
    //console.log('newMarginTop is ' + newMarginTop);
  };
}


function MenuHide()
{
  var marginTop = $(document).scrollTop() - $(window).height();
  var animationTime = 1;
  var timeForNavBorderDisappear = animationTime - ($("#nav").height() / $(window).height() * animationTime);

  var element = $('#menu');

  var menuDisappears = gsap.timeline();
  menuDisappears.to($('.menu-item'), 0.5*animationTime, {opacity:0});
  menuDisappears.to(element, 0.5*animationTime, {marginTop:marginTop + 'px', opacity:0});
  menuDisappears.play();

  $('#nav').removeClass('noborder');

  //show scroll bar after animations complete
  ScrollStart();
}


function MenuShow()
{
  MenuShowInit();

  var marginTop = $(document).scrollTop() - $(window).height();
  var newMarginTop = marginTop + $(window).height();
  var animationTime = 1;
  var timeForNavBorderDisappear = $("#nav").height() / $(window).height() * animationTime;

  var element = $('#menu');

  //show menu
  var menuDropsDown = gsap.timeline();
  menuDropsDown.to(element, 0, {marginTop:marginTop + 'px', opacity:0});
  menuDropsDown.to(element, animationTime, {marginTop:newMarginTop + 'px', opacity:1});
  menuDropsDown.to($('.menu-item'), animationTime, {transform:'translateY(0px)', opacity:1});
  menuDropsDown.play();

  //hide border about when menu hits it
  $('#nav').addClass('noborder');

  ScrollStop();
}


function MenuShowInit()
{
  $('.menu-item').css('opacity', 0);

  gsap.set('#menu-home', {transform:'translateY(5px)'});
  gsap.set('#menu-about', {transform:'translateY(10px)'});
  gsap.set('#menu-work', {transform:'translateY(15px)'});
  gsap.set('#menu-services', {transform:'translateY(20px)'});
  gsap.set('#menu-blog', {transform:'translateY(25px)'});

}


function ScrollStart()
{
  var html = jQuery('html');
  var scrollPosition = html.data('scroll-position');
  html.css('overflow', html.data('previous-overflow'));
  window.scrollTo(scrollPosition[0], scrollPosition[1])
}


function ScrollStop()
{
  var scrollPosition = [
    self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
    self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
  ];
  var html = jQuery('html'); // it would make more sense to apply this to body, but IE7 won't have that
  html.data('scroll-position', scrollPosition);
  html.data('previous-overflow', html.css('overflow'));
  html.css('overflow', 'hidden');
  window.scrollTo(scrollPosition[0], scrollPosition[1]);
}

